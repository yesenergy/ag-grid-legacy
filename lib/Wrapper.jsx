import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import * as AgGridEnterprise from 'ag-grid-enterprise';

const AgGridReactLegacy = (props) => {
	return (
		<AgGridReact {...props} ></AgGridReact>
	);
};

function setLicenseKey(licenseKey) {
	AgGridEnterprise.LicenseManager.setLicenseKey(licenseKey);
}

export default { AgGridReactLegacy, setLicenseKey };